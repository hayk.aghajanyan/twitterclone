import {
    Container,
    createStyles,
    Grid,
    InputBase,
    makeStyles,
    Paper,
    Theme,
    Typography,
    withStyles,
} from '@material-ui/core';
import React from 'react';

import grey from '@material-ui/core/colors/grey';
import { Tweet } from '../components/Tweet';
import { SideMenu } from '../components/SideMenu';

export const useHomeStyles = makeStyles((theme: Theme) => ({
    wrapper: {
        height: '100vh',
    },
    logo: {
        margin: '10px 0',
    },
    logoIcon: {
        fontSize: 36,
    },
    sideMenuList: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
        width: 230,
    },
    sideMenuListItem: {
        cursor: 'pointer',
        '&:hover': {
            '& div': {
                backgroundColor: 'rgba(29, 161, 242, 0.1)',
                '& h6': {
                    color: theme.palette.primary.main,
                },
                '& svg path': {
                    fill: theme.palette.primary.main,
                },
            },
        },

        '& div': {
            display: 'inline-flex',
            alignItems: 'center',
            position: 'relative',
            padding: '0 25px 0 20px',
            borderRadius: 30,
            height: 50,
            marginBottom: 15,
            transition: 'background-color 0.1s ease-in-out',
        },
    },
    sideMenuListItemLabel: {
        fontWeight: 700,
        fontSize: 20,
        marginLeft: 15,
    },
    sideMenuListItemIcon: {
        fontSize: 32,
        marginLeft: -5,
    },
    sideMenuTweetButton: {
        padding: theme.spacing(3.2),
        marginTop: theme.spacing(2),
    },
    tweetsWrapper: {
        borderRadius: 0,
        height: '100%',
        borderTop: '0',
        borderBottom: '0',
    },
    tweetsHeader: {
        borderTop: '0',
        borderLeft: '0',
        borderRight: '0',
        borderRadius: 0,
        padding: '10px 15px',
        '& h6': {
            fontWeight: 800,
        },
    },
    tweet: {
        cursor: 'pointer',
        paddingTop: 15,
        paddingLeft: 20,
        '&:hover': {
            backgroundColor: 'rgb(245, 248, 250)',
        },
    },
    tweetAvatar: {
        width: theme.spacing(5),
        height: theme.spacing(5),
    },
    tweetFooter: {
        display: 'flex',
        position: 'relative',
        left: -13,
        justifyContent: 'space-between',
        width: 450,
    },
    tweetUserName: {
        color: grey[500],
    },
}));

const SearchTextField = withStyles(() =>
    createStyles({
        input: {
            borderRadius: 30,
            backgroundColor: '#E6ECF0',
            height: 45,
            padding: 0,
        },
    }),
)(InputBase);

export const Home = () => {
    const classes = useHomeStyles();

    return (
        <Container className={classes.wrapper} maxWidth="lg">
            <Grid container spacing={3}>
                <Grid item xs={3}>
                    <SideMenu classes={classes} />
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.tweetsWrapper} variant="outlined">
                        <Paper className={classes.tweetsHeader} variant="outlined">
                            <Typography variant="h6">Главная</Typography>
                        </Paper>
                        {[
                            ...new Array(20).fill(
                                <Tweet
                                    text="According to press reports, Iran may be planning an assassination, or other attack, against the United States in retaliation for the killing of terrorist leader Soleimani, which was carried out for his planning a future attack, murdering U.S. Troops, and the death & suffering..."
                                    user={{
                                        fullname: 'Donald Trump',
                                        username: 'DonaldTrump',
                                        avatarUrl:
                                            'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFhUWFRoWGBgYFhYWFxoYFxUWFxoXFxoZHSggGholHRcaITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGxAQGi0lICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIANgA6QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABAUDBgcCAQj/xABAEAABAwICCAMGBAQEBwEAAAABAAIDESEEMQUGEkFRYXGBkaHwBxMiMrHBFELR4VJicvEWI4KSJDM0Q2OishX/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAgMEAQX/xAAmEQACAgICAgEEAwEAAAAAAAAAAQIRAxIhMQRREzJBYYEicdEF/9oADAMBAAIRAxEAPwDuKIiAIiIAiIgCIiAIiIAiKLPpCNmbhXgLlcbS7FEpFTv1gYMgT3CwSaytH5Cq/mh7J6S9F+i11utTK/L5rJ/iZv8AD5p80PY+OXovkVTDrBC4XJHmp8GMY/5XA/VTUovpkWmuzOiIpHAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiw4rEtjaXPNAPVkBlJpmq+fS7BZvxHll4rVtN6zh/wizeHHqq6DSF758Fiy+XTqBdHF7NmxWOlfba2W8uHVQJRQW9dSVCOkCczlz9BY/wAUN5HSqzOcpdl0YV0e5Gmt14jBHMcLfdemY0fxDxKwy4zp/uP3TUsUZejzNEK1A8LHssYbTf2K8nSIGY9dsl9GKjO+nW481HUnqz0/D1uLI3bBqD5lZmSdD0z8N68unpvHceiO9U5RFot8FpSbLbPe/hVSxpuVp+KhHRUEeLAsR3BWUaQZkad7LRDO12USw+kbVhdPMNnCnMZK1ilDhUGoXPTM2tip+itKGN4vY5haYZk+yqWJo3ZF5jeHAEXBuvSvKgiIgCIiAIiIAiIgCIiAIiIASuc646c2n0FaCwW76bxgihc48KDuuW4klzto9hwWTyputUXYY82QAxzjW6yzOMYALrndXd0WSbE+7aSBU5Ac1Wx4V73bTjUlYVFI34obO30TYZhXiVOZNwUeLR5UlsNLKXJq1R6Divrnmi9CM702F22KRDeD2UaQHd67KzdESsBw9dyg0SVEBmIc3L9FIbji4XP3XubCE9lClwzhko20HBMktxZqhmJzuPXdRQw8PXRezUXupLkplGj23aGRNPFScNM4ZnzUWOUb/spkMeVGq2C5M+R0jpWqxJgBJrcq4UDQcOxAwHhXxU9enFUjzW7YREXTgREQBERAEREAREQBEWLFS7LHO4AlAabrlpEF/uwbNz6rTZsSK5rJpXEl7nvOQPW5VPCHOca57h+q8ybc5WbscUlROazaNFbQRABR8BFS28+v0Vq0NHzAd/0Vf3NipI+QnipDGtUeXSEbc9mnrkvjMfC7I8/XgpUdWVEt7AsTYvX6L66UE529XXxrq1vXh4LupLa0ffc36/VHQjIdSsglaBWuQr2VfpDHNG+1KeYv9VLUi5tGSVrQK1/foojyzLfwzP7Ki0hpMudRhysCK9yeZ+yx6PdISag9rAcAq3jZH5WXssAPCvrJRGRcVlgifn8LeNzu5ZLOGAk3tnXmoU0yxS2XJWnC0dmVsOjgKNPTx/RQZ46iozopEMuy0Hgb8KK9ccmbJyqOoYZ1WNPED6LIoeiX1hYeX0JCmL0UeWwiIugIiIAiIgCIiAIiIAqPXHGe7wzuLrK8Wm+0Wf4Gs43UMjqLJR7NZj0eHYZtK0cS5x3lYYsC1orYX6q5a0GACthY7hYXUJ7QW3+lMsqLNS1L8bexFY8MJJvTJU2N0hK/5fG5+v6Kdi3Xp63qLiMaxrRssL3VoGjMuJoB++5Z4xNnZTjRmIeagV6W+qyDB4qL4tknf8tVa4jS8UFG4mctecocOKkWyLiC5x6bPRZodZIXkNhnftkf8vEN2aj+UkD7q5RDdFVDp2SoDrHh9ls+i53yNA35nhfIeS1fT7muHyGKQbqAA+Fj2K2XVl52G2zA8woyVE4SslYzAvpUOLSBY5juN48FqM8Ujn7BNDXt/ZdAxotWluK0rF4eQzHYaabNS/JrRvJPZch+SWT8EvB4SKLZbT3kpvsjKnM7hzKsDLUU2YweDZgXDxaL2Wo/gJp45PculDQ4Bz6OD5f4tneGgZUzUDRmqMrpG7cOzR/zMEras3h20bu5513q5QtWZpZFB6tWba+YsfsOBaTcB1q8wQaH7cFmim/v+qgN0RiGktkJfEDWMkgvHI8ufNSmRObZ7XU40++Sz5EaE0mSZpKU4ceazzECGvMj6H7FQ8Uf8smmSwY7E/8ABnjX7foV1P8AiV5I8nV9WJQ7CxEGvw361urRaH7ItIGTCuY6+w63Q/2W+LfjltFM83JHWTQREUyAREQBERAEREAREQBaD7SgQWHdT7rflpftRcBh2cdq3Sl/sq8v0MnD6ka/BjKQEDMut4qLFJetTzVJHjjsCnAdrmpUrRj6gntRZNv4mvHCpDStdwqT38lBw+BmcxxiFJPy1tskigPnVbBgoS91K0U6NogfSpdUVFfOnVQjRe7RpzdQWljfevaX0O074iSXZ3FKcls+gdTYYmOYW+8Dm7FHA0Da1IFSd96qybiR8wHl6IWV8sr7Ntz3futKnxRRKFy2rki4TQ8UDCz4ntP5ZHe8A5DaCwR/C4AU4CnJSsU8to3OgzPHjRRsANp9iDf6ZqiTtmqEa5ZbzsqzeqrDOp64K6obA7uipcbI1jzXImlea5RPhos2NjoC42PUea9PgZSoJ7OJ+6iYaOoIzHBSW4Nn83QmysWS0USxNMxtLWg1yPG5WA4ZpbUAivq6lFrAflru5dF9fiQRRUylZbGDRr+LZmKqlxP/ACQ3+c/QK8xr7nkqTEM8NrwUV0dyLovfZPiBFiJITYSNsObb/Sq6yuHaLn93Kx4N2uBC7dDKHNDhcOAI6EVWzxpXGvR5vkxqV+z2iItJnCIiAIiIAiIgCIiALm/tWxBJZGMg2p7n9l0hcv8Aa4dl7HDe2/YqrP8AQWYvrRz3ES/C1o3n191sOjBQEn11Ws4OYE5X8q9VsuCdenLzWNLg31TLnR8gDlcSSNNyAeq1BmK2XFWMOOraqgi+i/hkbmGgdlNEgPdUuDn43KmfiN2/14KSOuJG0+B7txrRwFr081psenhhniNxG0Bc8SbmndXOsWKc8hjbnh04rlunoZMRiAxgJftdxTeeC6lcg7UTobtc65OqTlT9FB0ZrMyaR0byHD5TxqDWx3EHetewereJDmh4HUVJ7r2/UmaF4fAC7i2tD2qpcHFdo6houejdhxqR+bIEbrdPNWR5LVtE4d7I2k12h8wr91sEE4Ivb1koOiZ8xUtlVz4yhU/FZUyVPI3z5KJI8B1TfeVjnjFXA5EAqQGCyjaWL9hxYKua3apxpn5LkeGQmrRWOBBF6Cvqi7RqrJXCxf008CQuRRNE0bXbNN9Oa63qm2mGZ3Wrx/qZk8yNQRcIiLWecEREAREQBERAEREAWme0nQbp4mvY0uMddpozpx7b1uaKMo7KjsZauz80YHAP97I2Sth8IyAobCnRbDhn/EQbECpCutN6IEWKdQcfrbyoqPR+CAxEzj8zmNA/3VKwtNcHr8SWyPs8dTVZ/ckUPnfwXuX6KRCagVvatFAlEkYE1od/rNWJu2nr1zVbE3j5KbG/duQnZ9wOCAq4jOw3r3Hoxm1tbIzubAm2/isk+J2QKes1SaS1pjiozaFa2vv+5RK+CDdm0yQtoLC29eTAN3PctMGm8Y8/DDMQd/u3jwBF17h//QJLhC+n9cf0Lla4NkVFLmza3RDfvWH3ez6+qoP8RTxU/E4aRo/iA2mjm4sqArHDaehlb8L2mo4g/dV6tE9yXM6vr6qCY6nkp2Y9V5KPMzM+e7wUWySK+d1F4Y+rwOII7EfsvcgusWEp79gItX7FcXZ19Fho3ANbG4E5LpGgP+nj6H/6K0md7TZo+YU78fBb1oePZgjH8gPiK/dbMHbMHlu4omIiLSYAiIgCIiAIiIAiIgCIiA1XXXAigmplZ32P28FqWKwrCTsUvQ1PTiun6QwoljfGcnCnfcfFctniMZLCcjfjy7hZs0ado3+NO46sqa12uK8YaQ1osbpQHuANl6Z8wWNmyPBbYe/ryWTFHYFfBYsK6t6egpMoa4EEbqZeqroNR0njZp5fcwfNSrnEfCxvEn7ZnksOHxAwJOxFty75XN2nk8ATZo5CndbGGMhHwgAnPiepXiHAvku4DZrvpfxTb7Ia/c1ybXjGk/CCO3NYv8Q415vtEbr/ALroOj9EwjJjS4ZkgE9uGW5TzA2lNlvqqso7ZpWjMfLYyOUvEaNglPvG/wCXL/E220f525O735q9l0Qx16U6b+yiu0Js3BNa8VCTaCSJejgWtAdnxBt24Ji+AUJzHMz72+q9fibHLkFzs50RZDn63+vBQTjBHI15yBPmCPusuJkuSOKrMS7aooLsnJ0rNx0AH4yUe7FI202nnIA7hxK6c0UFAtc1BwIjwrTvedo9rD7rZF6eKNRPHz5N5BERWFIREQBERAEREAREQBERAFoHtE0G4/50R2aj4t9+K39YcVhw9pacioyjsqJQk4u0fm2Fr45nB7iSfC37VVvBPkrTXTQRilJAWtbey5YJwpnpYsm3ZskWKyv4ZqZh5togAH+28rWsPNwuVZYbFiMVN63UC+zaMPhm5mh7eqqTPstbs2AK052sVDY1U1mnA8C9xl+y7FUdbssonbLqVaQMj6rU/wBlKOLr+WwzzFuRWuY3SjSKg0OXA0I8VEOm6WFwbHPMblakRs2ts/BwFzS27cDe/wC6kzYsUvTLvktHbpXhRu+gpSmS+yaWrZtVyUCO6L+fSrSfWahTyg5Znktena6pcD3N7j6rNhp9kXrU2r3y6fooa0N7JuKkpetfW5YIW1AJ3nyXgVkcG7t6nBvx7POyilycyy4O0aEh2MPE3eI216kVKmrDg3VjYeLR9Asy9NdHksIiLpwIiIAiIgCIiAIiIAiIgCIiArNN6FjxLdl1juIXHdetA/hZtlp2hQEEimY3+BXdVo/tR0ZtwtmA+Q7Lv6XH4T2db/Uqc0LjZfgnUqOOxTEFeMZpEuFOHBZZWUKivhus0Ubm2ecHESalZ5pHNNQaL3hnUsbrJKF1M40V8k9c/wBV4bM42AJ52ClEcF7giNVaip/lmCIOBv8Aop2Had58FhcRVZ8OUZyJlc405eqLBdZqeC+tbkq2qRauWWeh2UuVt2rOrpnkLzZgIvzzoFrGEjo0c11XUdhGGAdY7Rd2cAQo4Y7S5OeS6gXsUYa0NGQFPBe0RbzzQiIgCIiAIiIAiIgCIiAIiIAiIgC57r/psPxuE0Y01ElZpx/42hwjZ3eC7iPdt4qJ7Tvam3Ak4bChsmJ/O43ZFXcQPmf/AC7t/A849knvcXpSXEzSGSQRlznOuS5xAHQACgAsAAAupWck6Rb6z6Edh3kEVYfkdy4Hn9VQE913XSGj2Txlj2ggrmGsep0kFXR1fH4kdQs88NO0asXkqSqXZrbW3rVZZQornObkpd3MB8VW0aEzFE2vbovcz9kVXyDddeNKCw5lTRGSMUbq8KZ/e6sYQq+AGvaqsIMq8PM5IzqR9JSIVcvN7q00HoeWd1GC29xyH7qDi5cI6pqPLLvQOAM8gYB8AoXngP4epW160a2t0ZGJnxOkiLgxwYQHN4EVseFKjcpeisCyCMMb3O8neSqrXvRX4rAzxD5i0ub/AFNuFox49EYs2b5Jfg2/QWmYcXAzEYd4fG8WORBFi1wzDgcwrBflH2X60zYDGs2WySMe7YkgZUl9bVazfI03HQioqv1a0qZWfUREAREQBERAEREAREQBERAYsS9zWOLG7bgKhtQ2p4VNh3XDNdvappEPfh2wfgnCx2vjmpuLXEBoB4gHk5d4VTrHq3hcdH7vExB4Hym7XtPFjxdvY0O+q6gfkSdxcSXEkkkkkkkkmpJJuSTvW+ew/FBmOkYf+5Fb/SQfurjWv2L4mKr8G/8AEMz2HbLJgOtmP/8AXoVp2pXvMJpSETRvicHFrmvaWOo4EZG9FYqZXPo/SLQj2VXiOSoWVpXGQRqentToZauZ/lv4jI9QtFx+hpcNUSNq0/maKt77wu0mKqh4vRweCCKgqtwTLoZZROGvjoa1qDvrbssOkn1DTurfruXRNK+zxziTA8MrmCKt8Fz3WXQb8O/3UmJwxdmWh5a4dWkHZPUriwyk6SNCzxfZEjm+KvK6tvebLWihLnEANAqSeQ3rzq/qxJMdrbjYz+Mva4dtk37kLrer2qEOGAd88hHzuv8A7Rk0dFx43ZyWZJGqau6nPfR+IGwMxHX4v9RGXQeK3zDYZsbQ1jQAOAUsxUXxsJOQqppJGWU3J8mAr7BA55oB14d1YQ6N3vPYfcqexgAoBQJZ1RNZ1V1EwmBkknjZtTyuc4yOuWhxrsRj8rfM7zkBtCIuEwiIgCIiAIiIAiIgCIiAIiIAiIgCh6R0VBOAJomSbJ2m7bQ4tP8AE0m7TzCIgMbtEs/KS3zHnfzXj8A4cD5L4i7syOiM7ITvCyloAJNgBUlEXbOOJzHW/WDSGKcYNHwyRwXa6c0jkk3ER7RDmM/mpU7qb9Dn9nuNFzhXurcltHkk8aGpKIvQwefLx1UYR/d2/wB2USw/I+WyTo/2a41+WFLQd7yxniCa+S6lqPq5jsKAyaaMw0tHV0hbw2HEDZHK4X1E8n/qZM8dHGKX9f6dx+LGD2tv9m4DDt33WUBEXmmij6iIh0IiIAiIgCIiAIiID//Z',
                                    }}
                                    classes={classes}
                                />,
                            ),
                        ]}
                    </Paper>
                </Grid>
                <Grid item xs={3}>
                    <SearchTextField placeholder="        Поиск по Твиттеру" fullWidth />
                </Grid>
            </Grid>
        </Container>
    );
};
