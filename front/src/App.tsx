import React from 'react';
import { Switch, Route } from "react-router-dom";
import { SignIn } from "./pages/SignIn";
import { Home } from "./pages/Home"

function App() {
  return (
    <div className="App">
        <Switch>
            <Route path="/" component={Home} exact/>
            <Route path="/signin" component={SignIn}/>
        </Switch>
    </div>
  );
}

export default App;
